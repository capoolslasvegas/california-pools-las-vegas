California Pools is one of the largest pool builders in the country and brings award-winning custom pools to the Las Vegas area. As seen on Pool Kings, we offer expert design solutions and innovative construction techniques to provide our customers with the highest-quality pool or backyard anywhere.

Address: 9975 S Eastern Ave, #115, Las Vegas, Nevada 89183, USA

Phone: 702-407-6830

Website: https://californiapools.com/locations/las-vegas
